package com.bonilla.numeroaleatorio;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView miTexto;
    Button miBoton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        miTexto = findViewById(R.id.textView);
        miBoton = findViewById(R.id.button);

        miBoton.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        Random generador= new Random();
        int numero;
        numero =generador.nextInt(20);
        miTexto.setText(Integer.toString(numero));


    }
}